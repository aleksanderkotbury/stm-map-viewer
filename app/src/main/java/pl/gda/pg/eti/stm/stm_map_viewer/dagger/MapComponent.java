package pl.gda.pg.eti.stm.stm_map_viewer.dagger;

import javax.inject.Singleton;

import dagger.Component;
import pl.gda.pg.eti.stm.stm_map_viewer.MainActivity;

@Singleton
@Component(modules = {MapModule.class})
public interface MapComponent {

    void inject(MainActivity mainActivity);
}
