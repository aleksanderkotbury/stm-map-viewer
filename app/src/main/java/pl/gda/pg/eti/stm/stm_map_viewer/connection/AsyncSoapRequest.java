package pl.gda.pg.eti.stm.stm_map_viewer.connection;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.schedulers.Schedulers;

@Singleton
public class AsyncSoapRequest {

    public static final String SOAP_URL = "http://" + ServerUrl.ADDRESS + "/ws";
    public static final String NAMESPACE = "http://eti.pg.gda.pl/stm/map";

    @Inject
    public AsyncSoapRequest() {
    }

    public Observable<SoapObject> requestSoap(String soapAddress, PropertyInfo... propertyInfos) {
        final SoapObject request = new SoapObject(NAMESPACE, soapAddress);

        for (PropertyInfo propertyInfo : propertyInfos) {
            request.addProperty(propertyInfo);
        }

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);

        return Observable.fromCallable(() -> doRequest(envelope))
                .subscribeOn(Schedulers.io());
    }

    private SoapObject doRequest(SoapSerializationEnvelope envelope) {
        HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_URL);
        try {
            httpTransportSE.call("", envelope);
            return (SoapObject) envelope.bodyIn;
        } catch (IOException | XmlPullParserException e) {
            throw new RuntimeException("sth went wrong with soap client", e);
        }
    }
}
