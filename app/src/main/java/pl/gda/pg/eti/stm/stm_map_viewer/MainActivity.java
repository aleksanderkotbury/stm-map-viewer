package pl.gda.pg.eti.stm.stm_map_viewer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ToggleButton;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.gda.pg.eti.stm.stm_map_viewer.dagger.DaggerActivity;
import pl.gda.pg.eti.stm.stm_map_viewer.dagger.MapComponent;
import pl.gda.pg.eti.stm.stm_map_viewer.map.MapBoundaries;
import pl.gda.pg.eti.stm.stm_map_viewer.map.MapBoundariesBuilder;
import pl.gda.pg.eti.stm.stm_map_viewer.map.MapClient;
import pl.gda.pg.eti.stm.stm_map_viewer.map.MapRestClient;
import pl.gda.pg.eti.stm.stm_map_viewer.map.MapSoapClient;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends DaggerActivity {

    @Inject
    MapSoapClient mapSoapClient;
    @Inject
    MapRestClient mapRestClient;
    private MapClient mapClient;
    @Inject
    NoConnectionImageProvider noConnectionImageProvider;

    @BindView(R.id.longitudeBar)
    RangeSeekBar<Double> longitudeRangeBar;
    @BindView(R.id.attitudeBar)
    RangeSeekBar<Double> attitudeRangeBar;
    @BindView(R.id.imageView)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState, MapComponent mapComponent) {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mapComponent.inject(this);
        mapClient = mapSoapClient;
        setupThumbnail();
        setupMapBoundaries();
    }

    @OnClick(R.id.toggleButton)
    void onToggleButton(ToggleButton toggleButton) {
        boolean checked = toggleButton.isChecked();
        if (checked) {
            mapClient = mapSoapClient;
        } else {
            mapClient = mapRestClient;
        }
    }

    private void setupMapBoundaries() {
        mapSoapClient.getMapBoundaries()
                .onErrorReturn(t -> MapBoundaries.DEFAULT)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setupMapBoundaries);
    }

    private void setupMapBoundaries(MapBoundaries mapBoundaries) {
        longitudeRangeBar.setRangeValues(mapBoundaries.getMinLongitude(), mapBoundaries.getMaxLongitude());
        attitudeRangeBar.setRangeValues(mapBoundaries.getMinAttitude(), mapBoundaries.getMaxAttitude());
        longitudeRangeBar.setOnRangeSeekBarChangeListener(this::changeMap);
        attitudeRangeBar.setOnRangeSeekBarChangeListener(this::changeMap);
    }

    private void changeMap(RangeSeekBar<Double> bar, double minValue, double maxValue) {
        MapBoundaries mapBoundaries = new MapBoundariesBuilder()
                .withMinLongitude(longitudeRangeBar.getSelectedMinValue())
                .withMaxLongitude(longitudeRangeBar.getSelectedMaxValue())
                .withMinAttitude(attitudeRangeBar.getSelectedMinValue())
                .withMaxAttitude(attitudeRangeBar.getSelectedMaxValue())
                .build();
        loadImage(mapBoundaries);
    }

    private void loadImage(MapBoundaries mapBoundaries) {
        mapClient.getMap(mapBoundaries)
                .onErrorResumeNext(t -> noConnectionImageProvider.getImage())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imageView::setImageBitmap);
    }

    private void setupThumbnail() {
        mapClient.getThumbnail()
                .onErrorResumeNext(t -> noConnectionImageProvider.getImage())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imageView::setImageBitmap);
    }
}
