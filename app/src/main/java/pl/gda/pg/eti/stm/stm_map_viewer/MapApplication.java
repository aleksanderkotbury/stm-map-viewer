package pl.gda.pg.eti.stm.stm_map_viewer;

import android.app.Application;

import pl.gda.pg.eti.stm.stm_map_viewer.dagger.DaggerMapComponent;
import pl.gda.pg.eti.stm.stm_map_viewer.dagger.MapComponent;
import pl.gda.pg.eti.stm.stm_map_viewer.dagger.MapModule;

public class MapApplication extends Application {

    private MapComponent mapComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mapComponent = DaggerMapComponent.builder()
                .mapModule(new MapModule(this))
                .build();
    }

    public MapComponent getMapComponent() {
        return mapComponent;
    }
}
