package pl.gda.pg.eti.stm.stm_map_viewer.bar;

import android.content.Context;
import android.util.AttributeSet;

import org.florescu.android.rangeseekbar.RangeSeekBar;

public class DoubleRangeBar extends RangeSeekBar<Double> {
    public DoubleRangeBar(Context context) {
        super(context);
    }

    public DoubleRangeBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DoubleRangeBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected String valueToString(Double value) {
        return value.toString();
    }

    @Override
    protected Double normalizedToValue(double normalized) {
        double v = absoluteMinValuePrim + normalized * (absoluteMaxValuePrim - absoluteMinValuePrim);
        return Math.round(v * 100000) / 100000d;
    }
}
