package pl.gda.pg.eti.stm.stm_map_viewer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.schedulers.Schedulers;

@Singleton
public class NoConnectionImageProvider {

    private final Observable<Bitmap> bitmap;

    @Inject
    public NoConnectionImageProvider(Context context) {
        bitmap = Observable.fromCallable(() -> getBitmap(context))
                .subscribeOn(Schedulers.io())
                .cache();
    }

    public Observable<Bitmap> getImage() {
        return bitmap;
    }

    private Bitmap getBitmap(Context context) {
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.no_network);
    }
}
