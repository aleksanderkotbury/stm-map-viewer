package pl.gda.pg.eti.stm.stm_map_viewer.dagger;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import pl.gda.pg.eti.stm.stm_map_viewer.MapApplication;

public abstract class DaggerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapComponent mapComponent = ((MapApplication) getApplication()).getMapComponent();
        onCreate(savedInstanceState, mapComponent);
    }

    protected abstract void onCreate(Bundle savedInstanceState, MapComponent mapComponent);
}
