package pl.gda.pg.eti.stm.stm_map_viewer.map;

import android.graphics.Bitmap;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import pl.gda.pg.eti.stm.stm_map_viewer.connection.AsyncSoapRequest;
import rx.Observable;

import static pl.gda.pg.eti.stm.stm_map_viewer.connection.AsyncSoapRequest.NAMESPACE;

@Singleton
public class MapSoapClient implements MapClient {

    public static final String GET_THUMBNAIL = "getMapThumbnailRequest";
    public static final String GET_DIMENSIONS = "getMapCoordinatesBoundariesRequest";
    public static final String BASE_64_IMAGE = "mapBase64";
    public static final String GET_MAP_REQUEST = "getMapRequest";

    private AsyncSoapRequest asyncSoapRequest;
    private Base64ToImageAsync base64ToImageAsync;

    @Inject
    public MapSoapClient(AsyncSoapRequest asyncSoapRequest, Base64ToImageAsync base64ToImageAsync) {
        this.asyncSoapRequest = asyncSoapRequest;
        this.base64ToImageAsync = base64ToImageAsync;
    }

    @Override
    public Observable<Bitmap> getThumbnail() {
        return asyncSoapRequest.requestSoap(GET_THUMBNAIL)
                .map(soap -> soap.getPrimitivePropertyAsString(BASE_64_IMAGE))
                .flatMap(base64ToImageAsync::getImage);
    }

    @Override
    public Observable<Bitmap> getMap(MapBoundaries mapBoundaries) {
        PropertyInfo leftTopCoordinate = new PropertyInfo();
        SoapObject leftTopValue = new SoapObject()
                .addProperty(NAMESPACE, "attitude", String.valueOf(mapBoundaries.getMinAttitude()))
                .addProperty(NAMESPACE, "longitude", String.valueOf(mapBoundaries.getMaxLongitude()));
        leftTopCoordinate.setName("leftTopCorner");
        leftTopCoordinate.setValue(leftTopValue);
        leftTopCoordinate.setNamespace(NAMESPACE);

        PropertyInfo rightDownCoordinate = new PropertyInfo();
        SoapObject rightDownValue = new SoapObject()
                .addProperty(NAMESPACE, "attitude", String.valueOf(mapBoundaries.getMaxAttitude()))
                .addProperty(NAMESPACE, "longitude", String.valueOf(mapBoundaries.getMinLongitude()));
        rightDownCoordinate.setName("rightDownCorner");
        rightDownCoordinate.setValue(rightDownValue);
        rightDownCoordinate.setNamespace(NAMESPACE);


        return asyncSoapRequest.requestSoap(GET_MAP_REQUEST, leftTopCoordinate, rightDownCoordinate)
                .map(soapObject -> soapObject.getPrimitivePropertyAsString(BASE_64_IMAGE))
                .flatMap(base64ToImageAsync::getImage);
    }

    public Observable<MapBoundaries> getMapBoundaries() {
        return asyncSoapRequest.requestSoap(GET_DIMENSIONS)
                .map(this::mapBoundariesObject);
    }

    private MapBoundaries mapBoundariesObject(SoapObject soapObject) {
        String minLongitude = soapObject.getPrimitivePropertyAsString("minLongitude");
        String maxLongitude = soapObject.getPrimitivePropertyAsString("maxLongitude");
        String minAttitude = soapObject.getPrimitivePropertyAsString("minAttitude");
        String maxAttitude = soapObject.getPrimitivePropertyAsString("maxAttitude");
        return new MapBoundariesBuilder()
                .withMinLongitude(Double.valueOf(minLongitude))
                .withMaxLongitude(Double.valueOf(maxLongitude))
                .withMinAttitude(Double.valueOf(minAttitude))
                .withMaxAttitude(Double.valueOf(maxAttitude))
                .build();
    }
}
