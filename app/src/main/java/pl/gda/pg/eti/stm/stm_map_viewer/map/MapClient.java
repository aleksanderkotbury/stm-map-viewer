package pl.gda.pg.eti.stm.stm_map_viewer.map;

import android.graphics.Bitmap;

import rx.Observable;

public interface MapClient {
    Observable<Bitmap> getMap(MapBoundaries mapBoundaries);
    Observable<Bitmap> getThumbnail();
}
