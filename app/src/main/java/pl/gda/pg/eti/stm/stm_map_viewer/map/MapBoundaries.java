package pl.gda.pg.eti.stm.stm_map_viewer.map;

public class MapBoundaries {

    public static final MapBoundaries DEFAULT = new MapBoundaries(0, 1, 0, 1);

    private final double minLongitude;
    private final double maxLongitude;
    private final double minAttitude;
    private final double maxAttitude;

    public MapBoundaries(double minLongitude, double maxLongitude, double minAttitude, double maxAttitude) {
        this.minLongitude = minLongitude;
        this.maxLongitude = maxLongitude;
        this.minAttitude = minAttitude;
        this.maxAttitude = maxAttitude;
    }

    public double getMinLongitude() {
        return minLongitude;
    }

    public double getMaxLongitude() {
        return maxLongitude;
    }

    public double getMinAttitude() {
        return minAttitude;
    }

    public double getMaxAttitude() {
        return maxAttitude;
    }
}
