package pl.gda.pg.eti.stm.stm_map_viewer.map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.inject.Inject;
import javax.inject.Singleton;

import pl.gda.pg.eti.stm.stm_map_viewer.connection.ServerUrl;
import rx.Observable;
import rx.schedulers.Schedulers;

@Singleton
public class MapRestClient implements MapClient {

    private static final String SERVER_URL = "//" + ServerUrl.ADDRESS;

    @Inject
    public MapRestClient() {
    }

    @Override
    public Observable<Bitmap> getMap(MapBoundaries mapBoundaries) {
        String urlString = buildMapFragmentRequest(mapBoundaries).toString();
        return Observable.fromCallable(() -> downloadImage(urlString))
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Bitmap> getThumbnail() {
        String thumbnailUrl = buildMapThumbnailRequest().toString();
        return Observable.fromCallable(() -> downloadImage(thumbnailUrl))
                .subscribeOn(Schedulers.io());
    }

    private Bitmap downloadImage(String urlString) {
        try {
            URL url = new URL(urlString);
            InputStream imageStream = url.openConnection().getInputStream();
            return BitmapFactory.decodeStream(imageStream);
        } catch (IOException e) {
            Log.e("MapResClient", "Error during network connection", e);
            throw new RuntimeException(e);
        }
    }

    private Uri buildMapThumbnailRequest() {
        return new Uri.Builder()
                .scheme("http")
                .encodedPath(SERVER_URL)
                .appendPath("maps")
                .appendPath("thumbnail")
                .build();
    }

    private Uri buildMapFragmentRequest(MapBoundaries mapBoundaries) {
        return new Uri.Builder()
                .scheme("http")
                .encodedPath(SERVER_URL)
                .appendPath("maps")
                .appendQueryParameter("leftTopAttitude", Double.toString(mapBoundaries.getMinAttitude()))
                .appendQueryParameter("leftTopLongitude", Double.toString(mapBoundaries.getMaxLongitude()))
                .appendQueryParameter("rightDownAttitude", Double.toString(mapBoundaries.getMaxAttitude()))
                .appendQueryParameter("rightDownLongitude", Double.toString(mapBoundaries.getMinLongitude()))
                .build();
    }
}
