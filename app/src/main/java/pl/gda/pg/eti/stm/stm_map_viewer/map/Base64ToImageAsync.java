package pl.gda.pg.eti.stm.stm_map_viewer.map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.schedulers.Schedulers;

@Singleton
class Base64ToImageAsync {

    @Inject
    public Base64ToImageAsync() {
    }

    Observable<Bitmap> getImage(String base64Map) {
        return Observable.fromCallable(() -> getImageObservable(base64Map))
                .subscribeOn(Schedulers.computation());
    }

    private Bitmap getImageObservable(String base64Map) {
        byte[] decodedString = Base64.decode(base64Map, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }
}
