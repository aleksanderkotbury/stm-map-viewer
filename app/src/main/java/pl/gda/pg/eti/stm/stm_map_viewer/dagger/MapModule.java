package pl.gda.pg.eti.stm.stm_map_viewer.dagger;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class MapModule {

    private final Application application;

    public MapModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context context() {
        return application;
    }
}
