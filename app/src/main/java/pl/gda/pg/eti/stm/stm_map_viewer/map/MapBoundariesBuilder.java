package pl.gda.pg.eti.stm.stm_map_viewer.map;

public class MapBoundariesBuilder {
    private double minLongitude;
    private double maxLongitude;
    private double minAttitude;
    private double maxAttitude;

    public MapBoundariesBuilder withMinLongitude(double minLongitude) {
        this.minLongitude = minLongitude;
        return this;
    }

    public MapBoundariesBuilder withMaxLongitude(double maxLongitude) {
        this.maxLongitude = maxLongitude;
        return this;
    }

    public MapBoundariesBuilder withMinAttitude(double minAttitude) {
        this.minAttitude = minAttitude;
        return this;
    }

    public MapBoundariesBuilder withMaxAttitude(double maxAttitude) {
        this.maxAttitude = maxAttitude;
        return this;
    }

    public MapBoundaries build() {
        return new MapBoundaries(minLongitude, maxLongitude, minAttitude, maxAttitude);
    }
}